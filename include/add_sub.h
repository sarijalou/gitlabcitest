#ifndef ADD_SUB_
#define ADD_SUB_

#ifdef __cplusplus
extern "C" {
#endif
    int add(int a, int b);
    int sub(int a, int b);
#ifdef __cplusplus
}
#endif
#endif