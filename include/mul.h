#ifndef MUL_
#define MUL_

#ifdef __cplusplus
extern "C" {
#endif
	int multiply(int a, int b);
#ifdef __cplusplus
}
#endif
#endif
